module fmq(
			clk,clk_100,clk_1k,
			Rst_n,
			fmq_en,fmq_en2,
			fmq
);
input clk;
input clk_100;
input clk_1k;
input Rst_n;
input fmq_en;input fmq_en2;
output fmq;


reg fmq=1'b0;reg en=0;

reg [7:0]cnt;
reg fmq_entemp;
wire fmq_temp;
assign fmq_temp=((fmq_entemp==0)&&(fmq_en==1)||(fmq_en2));

always@(posedge clk_100 or negedge Rst_n )
	if(!Rst_n)begin
		cnt=7'd0;en=0;
	end
	else begin
		fmq_entemp=fmq_en;
		if(cnt>=100)begin
			cnt=7'd0;en=0;
		end
		else if(fmq_temp==1)begin
			cnt=cnt+1;
			en=1;
		end
		else if(cnt!=7'd0)begin
			cnt=cnt+1;
			en=1;
		end
		else begin
			cnt=7'd0;
			en=0;
		end
	end

always@(posedge clk  )
begin
		if(en==1) fmq=clk_1k;
		else fmq=1'b0;
	end
	
endmodule
