module cnt(
			clk,
			Rst_n,
			clk_100,
			clk_1k,
			sec_clk
);
	input clk;

	input Rst_n;
	output clk_100;
	output clk_1k;
	output sec_clk;


	reg clk_100=1'b1;
	reg clk_1k=1'b1;
	reg sec_clk=1'b1;

	reg [15:0]cnt;	
	reg [15:0]cnt2;
	reg [27:0]cnt3;

	always@(posedge clk or negedge Rst_n)
	if(Rst_n == 1'b0)
		cnt <= 16'd0;
	else if(cnt == 16'd249999)begin
		cnt <= 16'd0;
		clk_100=!clk_100;end
	else
		cnt <= cnt + 1'b1;
		
	always@(posedge clk or negedge Rst_n)
	if(Rst_n == 1'b0)
		cnt2 <= 16'd0;
	else if(cnt2 == 16'd24999)begin
		cnt2 <= 16'd0;
		clk_1k=!clk_1k;end
	else
		cnt2 <= cnt2 + 1'b1;
		
	always@(posedge clk or negedge Rst_n)
	if(Rst_n == 1'b0)begin
		cnt3 <= 28'd0;
	end
	else if(cnt3 == 28'd24999999)begin
		cnt3 <= 28'd0;
		sec_clk=!sec_clk;
	end
	else
		cnt3 <= cnt3 + 1;	



endmodule
