module top(
			clk,
			Rst_n,
			iIR,
			seg_en,
			seg_data,
			seg_dp,
			fmq_out
			//,state,state_temp,duan
			);
			
			
	input clk;	//系统时钟，50M
	input Rst_n;	//全局复位，低电平复位

	input iIR;

	output [5:0]seg_en;
	output [6:0]seg_data;
	output seg_dp;
	output fmq_out;

	wire [15:0]irAddr;wire [15:0]irData;

	wire [41:0]disp_data;
	wire [5:0] disp_dp;

	wire seg_dp;
	wire fmq_out;
	wire smg_blinken;
	wire clk_100;
	wire clk_1k;
	wire sec_clk;

	
	smg smg(
			.clk(clk),
			.clk_1k(clk_1k),
			.Rst_n(Rst_n),
			.seg_en(seg_en),
			.seg_data(seg_data),
			.data_in(disp_data),
			.disp_dp(disp_dp),
			.seg_dp(seg_dp),
			.sec_clk(sec_clk),
			.smg_blinken(smg_blinken)

);	
	ir_decode ir_decode(
			.Clk(clk),
			.Rst_n(Rst_n),
			.iIR(iIR),
			.Get_Flag(Get_Flag),
			.irData(irData),
			.irAddr(irAddr)
);

	tanchishe tanchishe(
			.clk(clk),
			.Rst_n(Rst_n),
			.Get_Flag(Get_Flag),
			.irData(irData),
			.disp_data(disp_data),
			.disp_dp(disp_dp),
			.fmq_en(fmq_en),
			.fmq_en2(fmq_en2),
			.smg_blinken(smg_blinken)
);
	fmq fmq(
			.clk(clk),
			.clk_100(clk_100),
			.clk_1k(clk_1k),
			.Rst_n(Rst_n),
			.fmq_en(fmq_en),
			.fmq_en2(fmq_en2),
			.fmq(fmq_out)
);


	cnt cnt(
			.clk(clk),
			.Rst_n(Rst_n),
			.clk_100(clk_100),
			.clk_1k(clk_1k),
			.sec_clk(sec_clk)
);

endmodule

