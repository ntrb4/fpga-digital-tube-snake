module smg(
			clk,clk_1k,
			Rst_n,
			seg_en,
			seg_data,
			data_in,
			disp_dp,
			seg_dp,
			sec_clk,
			smg_blinken
);
	input clk;
	input clk_1k;
	input sec_clk;
	input Rst_n;
	input smg_blinken;
	
	input [5:0]disp_dp;
	output reg seg_dp;
	output reg [5:0]seg_en;
	output reg [6:0]seg_data;
	input [41:0]data_in;



reg [3:0]data;reg [2:0]cnt;reg [5:0]seg_en_temp;




always@(posedge clk_1k or negedge Rst_n)
	if(!Rst_n)begin
		cnt=3'd0;
	end
	else begin
		if(cnt==6) cnt=0;
		else cnt=cnt+1;
	end
//扫描
always@(posedge clk or negedge Rst_n)
	if(!Rst_n)begin
		seg_en=6'b111111;seg_data=7'b0000001;
	end
	else begin
		if(smg_blinken==1'b0)begin
		case(cnt)
			4'd0: begin seg_en<=6'b111110;seg_data<=data_in[41:35];seg_dp<=disp_dp[5];end
			4'd1: begin seg_en<=6'b111101;seg_data<=data_in[34:28];seg_dp<=disp_dp[4];end		
			4'd2: begin seg_en<=6'b111011;seg_data<=data_in[27:21];seg_dp<=disp_dp[3];end			
			4'd3: begin seg_en<=6'b110111;seg_data<=data_in[20:14];seg_dp<=disp_dp[2];end			
			4'd4: begin seg_en<=6'b101111;seg_data<=data_in[13:7];seg_dp<=disp_dp[1];end
			4'd5: begin seg_en<=6'b011111;seg_data<=data_in[6:0];seg_dp<=disp_dp[0];end
			default: seg_en<=6'b111111;
		endcase
		end
		else begin
		case(cnt)
			4'd0: begin seg_en<={5'b11111,sec_clk};seg_data<=data_in[41:35];seg_dp<=disp_dp[5];end
			4'd1: begin seg_en<={4'b1111,sec_clk,1'b1};seg_data<=data_in[34:28];seg_dp<=disp_dp[4];end		
			4'd2: begin seg_en<={3'b111,sec_clk,2'b11};seg_data<=data_in[27:21];seg_dp<=disp_dp[3];end			
			4'd3: begin seg_en<={2'b11,sec_clk,3'b111};seg_data<=data_in[20:14];seg_dp<=disp_dp[2];end			
			4'd4: begin seg_en<={4'b1,sec_clk,4'b1111};seg_data<=data_in[13:7];seg_dp<=disp_dp[1];end
			4'd5: begin seg_en<={sec_clk,5'b11111};seg_data<=data_in[6:0];seg_dp<=disp_dp[0];end
			default: seg_en<=6'b111111;
		endcase
		end
	end




	
endmodule
