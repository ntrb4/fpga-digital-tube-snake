module tanchishe(
	clk,
	Rst_n,
	Get_Flag,
	irData,
	disp_data,
	disp_dp,
	fmq_en,
	fmq_en2,
	smg_blinken
	//,state,state_temp,duan

);


	input clk;
	input Rst_n;
	
	
	input Get_Flag;
	input [15:0] irData;
	output [41:0] disp_data;
	output [5:0]disp_dp;
	output fmq_en;
	output fmq_en2;
	output smg_blinken;

	
	reg [6:0] disp [5:0];//位宽7，深度6的寄存器，用来存放数码管显示
	reg [3:0] wei_temp [15:0];//这两个位宽4，深度16的移位寄存器用来存放蛇头的路径
	reg [3:0] duan_temp [15:0];//分别存放数码管的位和段
	reg [15:0] state_temp;//存放上次接收到的红外数据
	reg [3:0] wei=4'd5;//蛇头的位码
	reg [3:0] duan=4'd6;//蛇头的段码
	reg [3:0] dp=4'd3;//食物的位置
	reg dead=1'b0;//死亡标志
	reg [5:0] disp_dp;//小数点显示
	reg fmq_en=1'b0;//吃食物的蜂鸣器使能
	reg fmq_en2=1'b0;//死亡的蜂鸣器使能
	reg [4:0]length=5'd1;//蛇的长度
	wire smg_blinken;//数码管闪烁使能
	
	assign smg_blinken=dead;
	
	localparam 
	right = 16'ha55a,
	left = 16'hf708,
	up = 16'he718,
	down = 16'had52;
	
	assign  disp_data[41:0] ={disp[5],disp[4],disp[3],disp[2],disp[1],disp[0]};
	integer i,j,k,l;
	initial begin
		for(i = 0; i <6; i = i + 1) begin
			disp[i] <= 7'b0000000;
		end
		for(i = 0; i <7; i = i + 1) begin
			wei_temp[i] <= 4'd8;
			duan_temp[i] <= 4'd8;
		end
		disp[duan][wei]=1;
	end
	
	always@(posedge Get_Flag)
	begin
		if(dead==1)begin
				wei=4'd5;
				duan=4'd6;
		end
		else begin
			case(irData)
			right:begin
				if((duan==1)&&(state_temp==up))begin
					duan=6;
				end
				else if((duan==1)&&(state_temp==down))begin
					duan=0;
				end
				else if((duan==2)&&(state_temp==up))begin
					duan=0;
				end
				else if((duan==2)&&(state_temp==down))begin
					duan=3;
				end
				else begin
					if(wei==0) wei=5;
					else wei=wei-1;
				end
			end
			left:begin
				if((duan==1)&&(state_temp==up))begin
					duan=6;
					if(wei==5) wei=0;
					else wei=wei+1;
				end
				else if((duan==1)&&(state_temp==down))begin
					duan=0;
					if(wei==5) wei=0;
					else wei=wei+1;
					end
				else if((duan==2)&&(state_temp==up))begin
					duan=0;
					if(wei==5) wei=0;
					else wei=wei+1;
					end
				else if((duan==2)&&(state_temp==down))begin
					duan=3;
					if(wei==5) wei=0;
					else wei=wei+1;				
				end
				else begin
					if(wei==5) wei=0;
					else wei=wei+1;	
					end

			end		
			up:begin
				if((duan==3)&&(state_temp==right))begin
					duan=2;
					if(wei==0) wei=5;
					else wei=wei-1;
				end
				else if((duan==3)&&(state_temp==left))begin
					duan=2;
				end
				else if((duan==0)&&(state_temp==right))begin
					duan=1;
					if(wei==0) wei=5;
					else wei=wei-1;
				end
				else if((duan==0)&&(state_temp==left))begin
					duan=1;
				end
				else if(duan==2)begin
					duan=1;
				end
			

				end
			down:begin
				if((duan==6)&&(state_temp==right))begin
					duan=1;
					if(wei==0) wei=5;
					else wei=wei-1;
				end
				else if((duan==6)&&(state_temp==left))begin
					duan=1;
				end
				else if((duan==0)&&(state_temp==right))begin
					duan=2;
					if(wei==0) wei=5;
					else wei=wei-1;
				end
				else if((duan==0)&&(state_temp==left))begin
					duan=2;
				end
				
				else if(duan==1)begin
					duan=2;
				end			
			end
			default:;
			endcase
		end
		wei_temp[0]=wei;duan_temp[0]=duan;
	end

	always@(posedge Get_Flag)
	begin
		if((irData==right)||(irData==left)||(irData==up)||(irData==down))begin
		state_temp<=irData;
		end
	end


	always@(posedge Get_Flag)
	begin
		if((irData==right)||(irData==left)||(irData==up)||(irData==down))begin
			wei_temp[15]=wei_temp[14];
			wei_temp[14]=wei_temp[13];
			wei_temp[13]=wei_temp[12];
			wei_temp[12]=wei_temp[11];
			wei_temp[11]=wei_temp[10];
			wei_temp[10]=wei_temp[9];
			wei_temp[9]=wei_temp[8];
			wei_temp[8]=wei_temp[7];
			wei_temp[7]=wei_temp[6];
			wei_temp[6]=wei_temp[5];
			wei_temp[5]=wei_temp[4];
			wei_temp[4]=wei_temp[3];
			wei_temp[3]=wei_temp[2];
			wei_temp[2]=wei_temp[1];
			wei_temp[1]=wei_temp[0];
			duan_temp[15]=duan_temp[14];
			duan_temp[14]=duan_temp[13];
			duan_temp[13]=duan_temp[12];
			duan_temp[12]=duan_temp[11];
			duan_temp[11]=duan_temp[10];
			duan_temp[10]=duan_temp[9];
			duan_temp[9]=duan_temp[8];
			duan_temp[8]=duan_temp[7];			
			duan_temp[7]=duan_temp[6];
			duan_temp[6]=duan_temp[5];
			duan_temp[5]=duan_temp[4];
			duan_temp[4]=duan_temp[3];
			duan_temp[3]=duan_temp[2];
			duan_temp[2]=duan_temp[1];
			duan_temp[1]=duan_temp[0];
		end
	end
	
		always@(negedge Get_Flag)
	begin
		for(k = 0; k <6; k = k + 1) begin
			disp[k] <= 7'b0000000;
		end			

		for(l = 0; (l <length)&&(l<15); l= l + 1) begin
			disp[wei_temp[l]][duan_temp[l]] <= 1;
		end
	end
	

	always@(negedge Get_Flag)
	begin
		dead=1'b0;
		for(j = 1; (j <(length))&&(j<15); j = j + 1) begin
			if((wei==wei_temp[j])&&(duan==duan_temp[j]))
				dead=1'b1;
			else ;
		end
		if(dead==1) begin
			fmq_en2=1'b1;
			length=1;
			dp=4'd3;
		end
		else if(((wei==dp)&&(duan==3)&&(state_temp==left))||
		((wei==(dp-1))&&(((duan==3)&&(state_temp==right))||
		((duan==2)&&(state_temp==up)))))
		begin
			if(dp>=4) dp=dp-3;
			else dp=dp+2;
			if(length<=16) length=length+1;
			fmq_en=1'b1;
		end
		else begin
			fmq_en=1'b0;
			fmq_en2=1'b0;
		end
	end
	
	
	always@(posedge clk)
	begin
		case(dp)
		4'd5:disp_dp=6'b100000;
		4'd4:disp_dp=6'b010000;		
		4'd3:disp_dp=6'b001000;		
		4'd2:disp_dp=6'b000100;		
		4'd1:disp_dp=6'b000010;	
		4'd0:disp_dp=6'b000001;
		endcase
	end	
endmodule
	